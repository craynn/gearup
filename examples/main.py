import os

def train(method, power, alpha: float=1e-3, flag: bool=False):
  """
  Trains method with foo.

  Actually, just prints `foo ** fee`.
  But, hey, it is an example.
  """
  print('%s: %.3e' % (method, alpha ** (power if flag else -power)))

def test1(method):
  """
  Tests method...

  A long
  several lines
  long
  description.
  """
  print('%s ... tested... kind of...' % (method, ))

def test2(method):
  """
  Undocumented test function.
  """
  print('%s ... tested... kind of... twice' % (method, ))

if __name__ == '__main__':
  root = os.path.dirname(os.path.dirname(__file__))
  config = os.path.join(root, 'examples', 'config/config.yaml')
  from gearup.config import read_config

  print(
    read_config(('foo.bar.bivalue=5', 'foo.bar=second', 'fee=three', 'fee.smth.new=novel'), config)
  )